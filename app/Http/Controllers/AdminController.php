<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bulk;
use App\email;
use Auth;
class AdminController extends Controller
{
    
   public function EMindex(){
      return view('welcome')->with("uname",Auth::user()->name);
   }
    public function saveData(Request $request){
         $idu = Auth::id();
         if ($request->input('submit') != null ){
            $file = $request->file('file');
            // File Details 
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();
      
            // Valid File Extensions
            $valid_extension = array("csv");
            // 2MB in Bytes
            $maxFileSize = 2097152; 
            // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){
              // Check file size
              if($fileSize <= $maxFileSize){
                // File upload location
                $location = 'uploads';
                // Upload file
                $file->move($location,$filename);
                // Import CSV to Database
                $filepath = public_path($location."/".$filename);
                // Reading file
                $file = fopen($filepath,"r");
                $importData_arr = array();
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                   $num = count($filedata );
                   if($i == 0){
                      $i++;
                      continue; 
                   }
                   for ($c=0; $c < $num; $c++) {
                      $importData_arr[$i][] = $filedata [$c];
                   }
                   $i++;
                }
                fclose($file);

                $bulkname = $request->name;

                $bulk = new bulk;
                $bulk->bulk_name= $bulkname;
                $bulk->user_id = $idu;
                $bulk->save();
                $bulk_id = $bulk->id;

                foreach($importData_arr as $importData){
                    $em = new email;

                    $id = $importData[0];
                    $name = $importData[1];
                    $number = $importData[2];
                    $email = $importData[3];

                    $em->name = $name;
                    $em->number = $number;
                    $em->email = $email;
                    $em->bulk_id = $bulk_id;

                    $em->save();

                }
                


                
               
              }else{
               // Session::flash('message','File too large. File must be less than 2MB.');
              }
      
            }else{
               //Session::flash('message','Invalid File Extension.');
            }
      
          }
          // Redirect to index
          return view('welcome');
        }
}
