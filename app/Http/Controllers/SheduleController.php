<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\schedul;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Auth;

class SheduleController extends Controller
{
    public function index(){
        $user_id= Auth::id();

        $bulks = DB::select('select * from bulks where user_id = '.$user_id);
        return view('admin')->with('bulks',$bulks)->with("uname",Auth::user()->name);
    }
    public function sendMail(Request $req){
        $user_id = $req->id;
        $alias = $req['alias'];
        $subject = $req['subject'];
        $mailbody = $req['mailbody'];
        $getDate = $req['getDate'];
        $bulk_id = $req['bulk'];
        
        $image = $req->file('select_file');
        $new_name = rand().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/attachments'), $new_name);

        $sch = new schedul;
        $sch->alies = $alias;
        $sch->subject	 = $subject	;
        $sch->message = $mailbody;
        $sch->url = $new_name;
        $sch->date = $getDate;
        $sch->status = 1;
        $sch->bulk_id = $bulk_id;
        $sch->user_id = $user_id;
        $sch->save();

        $emails = DB::select('select * from emails where bulk_id = '.$bulk_id);

        require '../vendor/autoload.php';
        

        foreach($emails as $mails){
            $mail = new PHPMailer;
            $mail->isSMTP();
            
            $mail->Host = 'smtp.mailtrap.io';                      // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'e99b7bc1cc8363';                    // SMTP username
            $mail->Password = 'fb88569ebf04a4';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 25;  
            $mail->setFrom('558895c892-cc4215@inbox.mailtrap.io', 'Udara');

            $ma = $mails->email;
            $mail->addAddress($ma, 'Mailer'); 
            $mail->Subject  = $subject;
            $mail->Body     = $mailbody;
            $mail->send();
        }
        // $msg = "notok";
        // if(!$mail->send()) {
        //     echo 'Message was not sent.';
        //     echo 'Mailer error: ' . $mail->ErrorInfo;
        // } else {
        // echo 'Message has been sent.';
        // $msg = "ok";
        // }
        return response()->json(['Msg'=>'Ok']);
    }
}