<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    public function login(Request $result){
        $email =  $result->get('email');
        $password =  $result->get('pwd');

        $userdata = array(
            'email' =>  $email ,
            'password' =>  $password
        );

            
            if (Auth::attempt($userdata)){
                return view('welcome')->with("uname",Auth::user()->name);;
            }else{
                return view('login')->with('Error',"Login Fail");
            }

        }

}
