<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'    => 'Kavidu Randika',
            'email'    => 'kavi@gmail.com',
            'password'   =>  Hash::make('456'),
            'remember_token' =>  str_random(10),
        ]);


    }
}
