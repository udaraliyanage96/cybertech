<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alies');
            $table->string('subject');
            $table->string('message');
            $table->string('url');
            $table->string('date');
            $table->string('status');
            $table->integer('bulk_id')->unsigned();
            $table->foreign('bulk_id')->references('id')->on('bulks');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduls');
    }
}
