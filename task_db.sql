-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2020 at 01:32 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cybertech`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulks`
--

CREATE TABLE `bulks` (
  `id` int(10) UNSIGNED NOT NULL,
  `bulk_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bulks`
--

INSERT INTO `bulks` (`id`, `bulk_name`, `user_id`, `created_at`, `updated_at`) VALUES
(9, 'My Batch', 2, '2020-09-21 07:18:02', '2020-09-21 07:18:02'),
(8, 'Bulk 1', 1, '2020-09-21 06:35:26', '2020-09-21 06:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bulk_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `name`, `number`, `email`, `bulk_id`, `created_at`, `updated_at`) VALUES
(26, 'Supun', '2', 'supun@gmail.com', 9, '2020-09-21 07:18:02', '2020-09-21 07:18:02'),
(25, 'Nuwan', '1', 'nuwan@gmail.com', 9, '2020-09-21 07:18:02', '2020-09-21 07:18:02'),
(24, 'udara', '3', 'ldudaraliyanage@gmail.com', 8, '2020-09-21 06:35:26', '2020-09-21 06:35:26'),
(23, 'priyadarshana', '2', 'ldudarapriyadarshana@gmail.com', 8, '2020-09-21 06:35:26', '2020-09-21 06:35:26'),
(22, 'iaLabs', '1', 'ialabs@gmail.com', 8, '2020-09-21 06:35:26', '2020-09-21 06:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_09_21_063630_create_bulks_table', 1),
(4, '2020_09_21_063651_create_emails_table', 1),
(5, '2020_09_21_084731_create_scheduls_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scheduls`
--

CREATE TABLE `scheduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `alies` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `bulk_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scheduls`
--

INSERT INTO `scheduls` (`id`, `alies`, `subject`, `message`, `url`, `date`, `status`, `bulk_id`, `user_id`, `created_at`, `updated_at`) VALUES
(56, 'Hi', 'y', 'aas', '8733.pdf', '2020-09-14', '1', 9, 2, '2020-09-21 07:23:43', '2020-09-21 07:23:43'),
(55, 'Hello', 'aa', 'asa', '25799.pdf', '2020-09-15', '1', 9, 2, '2020-09-21 07:21:55', '2020-09-21 07:21:55'),
(54, 'Hi', 'SUb', 'aaas', '5638.pdf', '2020-09-15', '1', 9, 2, '2020-09-21 07:20:47', '2020-09-21 07:20:47'),
(53, 'Hi', 'SUb', 'aaas', '22753.pdf', '2020-09-15', '1', 9, 2, '2020-09-21 07:19:47', '2020-09-21 07:19:47'),
(52, 'Hi', 'SUb', 'aaas', '16968.pdf', '2020-09-15', '1', 9, 2, '2020-09-21 07:19:27', '2020-09-21 07:19:27'),
(51, 'Hi', 'Ok', 'aasahs', '25835.png', '2020-09-22', '1', 8, 1, '2020-09-21 06:49:29', '2020-09-21 06:49:29'),
(50, 'aa', 'asa', 'sa', '12346.jpg', '2020-09-08', '1', 8, 1, '2020-09-21 06:44:58', '2020-09-21 06:44:58'),
(49, 'aa', 'asa', 'sa', '27493.jpg', '2020-09-08', '1', 8, 1, '2020-09-21 06:43:54', '2020-09-21 06:43:54'),
(48, 'Hi', 'Testing', 'Hello Guys', '15452.jpg', '2020-09-15', '1', 8, 1, '2020-09-21 06:42:05', '2020-09-21 06:42:05'),
(47, 'Hi', 'Testing', 'Hello Guys', '27343.jpg', '2020-09-15', '1', 8, 1, '2020-09-21 06:40:14', '2020-09-21 06:40:14'),
(46, 'Hi', 'Testing', 'Hello Guys', '20270.jpg', '2020-09-15', '1', 8, 1, '2020-09-21 06:39:23', '2020-09-21 06:39:23'),
(45, 'Hi', 'Testing', 'Hello Guys', '7593.jpg', '2020-09-15', '1', 8, 1, '2020-09-21 06:38:48', '2020-09-21 06:38:48'),
(44, 'Hi', 'Testing', 'Hello Guys', '32048.jpg', '2020-09-15', '1', 8, 1, '2020-09-21 06:37:58', '2020-09-21 06:37:58'),
(43, 'Hi', 'Testing', 'Hello Guys', '6771.jpg', '2020-09-15', '1', 8, 1, '2020-09-21 06:37:38', '2020-09-21 06:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Udara Liyanage', 'udara@gmail.com', '$2y$10$Idf/wEdupIhqwIU0vKR5WOSHKb61c7fZEofFapHNsELDNklvLi0W.', 'Xq1HFQe7iV', '2020-09-21 02:35:49', '2020-09-21 02:35:49'),
(2, 'Kavidu Randika', 'kavi@gmail.com', '$2y$10$QasTmQgEfHygmfDyK59rweTJ3Oaqc0AWTWLRAyWMSuqK3Lrm9/nYS', 'KeZpgvpIKX', '2020-09-21 07:07:02', '2020-09-21 07:07:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bulks`
--
ALTER TABLE `bulks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bulks_user_id_foreign` (`user_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emails_bulk_id_foreign` (`bulk_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `scheduls`
--
ALTER TABLE `scheduls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scheduls_bulk_id_foreign` (`bulk_id`),
  ADD KEY `scheduls_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bulks`
--
ALTER TABLE `bulks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `scheduls`
--
ALTER TABLE `scheduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
