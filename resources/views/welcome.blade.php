<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" type="text/css" href="css/welcome.css">
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Cyber-Tech Email Project</title>
       
    </head>
    <body>
        
    </body>
</html>
<html lang="en">
    <head>
		
	
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark nv">
            <a class="navbar-brand" href="home.php">Cyber-Tech</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0 col-lg-1" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="index.php">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="pro row">
                        <img src="" class="">
                        <div class="nm">
                           
                        </div>
                    </div>
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                        <h6 class="container" style="color:#fff">{{$uname}}</h6>

                            <a class="nav-link" href="/emailManager"><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>Email manager</a>
                            <a class="nav-link" href="/emailScheduler"><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>Email scheduler</a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Studio
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <div class="">
                            <label>Download Sample Exel File</label>
                            <div class="">
                                <a href="batch.csv"><input type="button" class="btn btn-success" value="Download"></a>
                            </div>
                        </div>
                        <hr>
                        <div class="">
                            <label>Upload Email Bulk</label>
                            <form method='post' action='/uploadExcel' enctype='multipart/form-data' >
                                {{ csrf_field() }}
                                <input type='text' name='name'  class="form-control col-lg-2 name" placeholder="Name Here">
                                <input type='file' name='file' class="btn btn-warning">
                                <input type='submit' name='submit' value='Import' class="btn btn-success">
                            </form>
                        </div>
                    </div>
                </main>
            </div>
        </div>
  
      
    </body>
</html>
