<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/welcome.css">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <title>Cyber-Tech Email Project</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--datatable-->

</head>

<body>

</body>

</html>
<html lang="en">

<head>


</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark nv">
        <a class="navbar-brand" href="home.php">Cyber-Tech</a>
        <button class="btn btn-link btn-sm order-1 order-lg-0 col-lg-1" id="sidebarToggle" href="#"><i
                class="fas fa-bars"></i></button>
        <!-- Navbar Search-->

        <!-- Navbar-->
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Settings</a><a class="dropdown-item" href="#">Activity Log</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php">Logout</a>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="pro row">
                    <img src="" class="">
                    <div class="nm">

                    </div>
                </div>
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <h6 class="container" style="color:#fff">{{$uname}}</h6>
                        <a class="nav-link" href="/emailManager">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>Email manager
                        </a>
                        <a class="nav-link" href="/emailScheduler">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>Email scheduler
                        </a>
                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logged in as:</div>
                    Studio
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <label>Bulk Email Send Section</label>
                    <form method="post" id="upload_form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="">
                            <input type="text" id="alias" name="alias" placeholder="Alias Here"
                                class="form-control col-lg-3" required>
                            <input type="text" id="subject" name="subject" placeholder="Subject Here"
                                class="form-control col-lg-4" required>
                            <textarea id="mailbody" name="mailbody" class="form-control col-lg-6" rows="10"
                                maxlength="50000" required></textarea>
                            <div class="row rw">
                                <div class="col-lg-3 file"><input type='file' name='select_file' id="select_file" class="btn btn-warning"></div>
                                <div class="col-lg-2"><input type="date" name="getDate" id="getDate"
                                        class="form-control ">
                                </div>
                            </div>
                            <select name="bulk" id="bulk" class="form-control col-lg-3">
                                <option value="dis" disabled selected>Select Bulk</option>
                                @foreach($bulks as $bulk)
                                <option value="{{$bulk->id}}">{{$bulk->bulk_name}}</option>
                                @endforeach
                            </select>
                            <input type="reset" name="cancle" value="Cancle" class="btn btn-danger" onclick="Reset()">
                            <input type="submit" name="submit" value="Send Emails" class="btn btn-primary">

                        </div>
                    </form>
                </div>
            </main>
        </div>
    </div>


</body>
<script>
    
    $('#upload_form').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/sendMail/"+ {{Auth::id()}},
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                console.log(data['Msg']);
                if(data['Msg']=="Ok"){
                    alert('Email sent Successfully');
                }else{
                    alert('Email sent Fail');
                }
                Reset();
            }
        })
    });
    $(document).ready(function () {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;
        document.getElementById("getDate").value = today;
    });

    function Reset() {
        document.getElementById("alias").value = "";
        document.getElementById("subject").value = "";
        document.getElementById("mailbody").value = "";
        document.getElementById("bulk").value = "dis";
    }
</script>

</html>