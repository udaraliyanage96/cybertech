<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/emailManager', function () {
//     return view('welcome');
// });
Route::get('/emailManager','AdminController@EMindex');

Route::get('/', function () {
    return view('login');
});

Route::get('/admin','AdminController@index');
Route::get('/emailScheduler','SheduleController@index');
Route::post('/uploadExcel','AdminController@saveData');
Route::post('/sendMail/{id}', 'SheduleController@sendMail');

Route::post('/login', 'LoginController@login');